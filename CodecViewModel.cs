﻿using Acrotech.PortableLogAdapter;
using Acrotech.PortableViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Acrotech.PortableSteganography
{
    public class CodecViewModel : BaseViewModel
    {
        private static readonly ILogger Logger = ServiceManager.LogManager.GetLogger<CodecViewModel>();

        public CodecViewModel()
        {
            Codec = new LSBSteganographyCodec();
        }

        public ISteganographyCodec Codec { get; private set; }

        #region VesselImageUri

        private Uri propVesselImageUri = default(Uri);

        public const string VesselImageUriPropertyName = "VesselImageUri";

        public Uri VesselImageUri
        {
            get { return propVesselImageUri; }
            set { RaiseAndSetIfChanged(ref propVesselImageUri, value, VesselImageUriPropertyName, OnVesselImageUriChanged); }
        }

        protected virtual void OnVesselImageUriChanged(Uri oldValue)
        {
            GetEncodeCommand().RaiseCanExecuteChanged();
        }

        #endregion

        #region Payload

        private string propPayload = default(string);

        public const string PayloadPropertyName = "Payload";

        public string Payload
        {
            get { return propPayload; }
            set { RaiseAndSetIfChanged(ref propPayload, value, PayloadPropertyName, OnPayloadChanged); }
        }

        protected virtual void OnPayloadChanged(string oldValue)
        {
            GetEncodeCommand().RaiseCanExecuteChanged();
        }

        #endregion

        #region Password

        private string propPassword = default(string);

        public const string PasswordPropertyName = "Password";

        public string Password
        {
            get { return propPassword; }
            set { RaiseAndSetIfChanged(ref propPassword, value, PasswordPropertyName, OnPasswordChanged); }
        }

        protected virtual void OnPasswordChanged(string oldValue)
        {
            GetEncodeCommand().RaiseCanExecuteChanged();
            GetDecodeCommand().RaiseCanExecuteChanged();
        }

        #endregion

        #region EncodedImageUri

        private Uri propEncodedImageUri = default(Uri);

        public const string EncodedImageUriPropertyName = "EncodedImageUri";

        public Uri EncodedImageUri
        {
            get { return propEncodedImageUri; }
            private set { RaiseAndSetIfChanged(ref propEncodedImageUri, value, EncodedImageUriPropertyName, OnEncodedImageUriChanged); }
        }

        protected virtual void OnEncodedImageUriChanged(Uri oldValue)
        {
            GetDecodeCommand().RaiseCanExecuteChanged();
        }

        #endregion

        #region Encode Command

        private DelegateCommand cmdEncode = null;

        public ICommand Encode
        {
            get { return GetEncodeCommand(); }
        }

        protected DelegateCommand GetEncodeCommand()
        {
            if (cmdEncode == null)
            {
                cmdEncode = new DelegateCommand(ExecuteEncode, CanEncodeExecute);
            }

            return cmdEncode;
        }

        protected bool CanEncodeExecute()
        {
            return 
                VesselImageUri != null &&
                string.IsNullOrEmpty(Payload) == false &&
                string.IsNullOrEmpty(Password) == false;
        }

        protected void ExecuteEncode()
        {
            var encodedImageUri = ServiceManager.UriDataService.CreateEncodedUri(VesselImageUri);

            using (var input = ServiceManager.UriDataService.OpenRead(VesselImageUri))
            using (var output = ServiceManager.UriDataService.OpenWrite(encodedImageUri))
            using (var payload = new DynamicLengthStream(Payload))
            {
                Codec.Encode(input, payload, output);
            }

            EncodedImageUri = encodedImageUri;
        }

        #endregion

        #region Decode Command

        private DelegateCommand cmdDecode = null;

        public ICommand Decode
        {
            get { return GetDecodeCommand(); }
        }

        protected DelegateCommand GetDecodeCommand()
        {
            if (cmdDecode == null)
            {
                cmdDecode = new DelegateCommand(ExecuteDecode, CanDecodeExecute);
            }

            return cmdDecode;
        }

        protected bool CanDecodeExecute()
        {
            return 
                string.IsNullOrEmpty(Password) == false &&
                EncodedImageUri != null;
        }

        protected void ExecuteDecode()
        {
            using (var payload = new DynamicLengthStream())
            {
                using (var input = ServiceManager.UriDataService.OpenRead(EncodedImageUri))
                {
                    Codec.Decode(input, payload);
                }

                var buffer = payload.ToDataArray();
                Payload = Encoding.UTF8.GetString(buffer, 0, buffer.Length);
            }
        }

        #endregion
    }
}
