﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acrotech.PortableSteganography
{
    public class DynamicLengthStream : Stream
    {
        public DynamicLengthStream(string content)
            : this(Encoding.UTF8.GetBytes(content))
        {
        }

        public DynamicLengthStream(byte[] buffer)
        {
            DataStream = new MemoryStream(buffer);
            LengthStream = new MemoryStream(BitConverter.GetBytes((int)DataStream.Length));
        }

        public DynamicLengthStream(Stream stream)
        {
            DataStream = new MemoryStream();
            stream.CopyTo(DataStream);
            stream.Flush();

            LengthStream = new MemoryStream(BitConverter.GetBytes((int)DataStream.Length));
        }

        public DynamicLengthStream()
        {
            DataStream = new MemoryStream();
            LengthStream = new MemoryStream(sizeof(int));
        }

        private MemoryStream DataStream { get; set; }
        private MemoryStream LengthStream { get; set; }

        public override bool CanRead
        {
            get { return LengthStream.Position < LengthStream.Capacity || DataStream.Position < DataStream.Capacity; }
        }

        public override bool CanSeek
        {
            get { return true; }
        }

        public override bool CanWrite
        {
            get { return LengthStream.Position < LengthStream.Capacity || DataStream.Position < DataStream.Capacity; }
        }

        public override void Flush()
        {
            DataStream.Flush();
            LengthStream.Flush();
        }

        public override long Length
        {
            get { return LengthStream.Length + DataStream.Length; }
        }

        public override long Position
        {
            get
            {
                return LengthStream.Position + DataStream.Position;
            }
            set
            {
                if (value > LengthStream.Capacity)
                {
                    LengthStream.Position = LengthStream.Capacity;
                    DataStream.Position = value - LengthStream.Capacity;
                }
                else
                {
                    LengthStream.Position = value;
                    DataStream.Position = 0;
                }
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            var read = 0;

            if (LengthStream.Position < LengthStream.Capacity)
            {
                read += LengthStream.Read(buffer, offset, Math.Min(count, LengthStream.Capacity));

                offset += read;
                count -= read;
            }

            if (count > 0)
            {
                read += DataStream.Read(buffer, offset, count);
            }

            return read;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            switch (origin)
            {
                case SeekOrigin.Begin:
                    Position = offset;
                    break;
                case SeekOrigin.Current:
                    Position = Position + offset;
                    break;
                case SeekOrigin.End:
                    Position = Length + offset;
                    break;
            }

            return Position;
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            if (LengthStream.Position < LengthStream.Capacity)
            {
                var write = Math.Min(count, LengthStream.Capacity);

                LengthStream.Write(buffer, offset, write);

                offset += write;
                count -= write;
            }

            if (LengthStream.Position == LengthStream.Capacity && DataStream.Capacity == 0)
            {
#if PCL
                DataStream.Capacity = BitConverter.ToInt32(LengthStream.ToArray(), 0);
#else
                DataStream.Capacity = BitConverter.ToInt32(LengthStream.GetBuffer(), 0);
#endif
            }

            if (count > 0)
            {
                DataStream.Write(buffer, offset, count);
            }
        }

        public byte[] ToDataArray()
        {
            return DataStream.ToArray();
        }
    }
}
