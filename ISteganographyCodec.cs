﻿using System;
using System.IO;

namespace Acrotech.PortableSteganography
{
    public interface ISteganographyCodec
    {
        int BitsPerPixel { get; }
        double PayloadByteRatio { get; }

        void Decode(Stream input, DynamicLengthStream payload);
        void Encode(Stream input, DynamicLengthStream payload, Stream output);
        int GetCapacity(Stream input);
        void GetOptimalImageSize(int payloadLength, double aspectRatio, out int width, out int height);
    }
}
