﻿# README #

This library provides portable steganography functions to transparently encode and decode encrypted data within a vessel image.

---

## Who do I talk to? ##

* Contact me for suggestions, improvements, or bugs

## Changelog ##

#### 1.0.0.0 ####

* Initial Release
