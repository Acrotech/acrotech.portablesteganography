﻿using Acrotech.PortableIoCAdapter;
using Acrotech.PortableLogAdapter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acrotech.PortableSteganography
{
    public static class ServiceManager
    {
        public static void Initialize(IContainer container)
        {
            Container = container;
            LogManager = Container.ComposeOrDefault<ILogManager>(PortableLogAdapter.Managers.DebugLogManager.Default);
            UriDataService = Container.Compose<IEncodingService>();
        }

        public static IContainer Container { get; private set; }
        public static ILogManager LogManager { get; private set; }
        public static IEncodingService UriDataService { get; private set; }
    }
}
