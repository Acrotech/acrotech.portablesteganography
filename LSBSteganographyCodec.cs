﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acrotech.PortableSteganography
{
    public class LSBSteganographyCodec : ISteganographyCodec
    {
        public const string ConfigBlueChannelBits = "BlueChannelBits";
        public const string ConfigGreenChannelBits = "GreenChannelBits";
        public const string ConfigRedChannelBits = "RedChannelBits";
        public const string ConfigAlphaChannelBits = "AlphaChannelBits";

        // these defaults give maximum capacity with minimal image distortion 
        // (slightly noticeable if you compare both original and encoded images side by side)
        // lower numbers will give even less distortion
        public const double DefaultBlueChannelBits = 3;
        public const double DefaultGreenChannelBits = 2;    // human eyes are more sensitive to green so reduce the usual bits here by one
        public const double DefaultRedChannelBits = 3;
        public const double DefaultAlphaChannelBits = 0;

        public LSBSteganographyCodec(IDictionary<string, double> config = null)
        {
            LoadConfig(config);
        }

        public int BlueChannelBits { get; set; }
        public int GreenChannelBits { get; set; }
        public int RedChannelBits { get; set; }
        public int AlphaChannelBits { get; set; }

        public int BitsPerPixel { get { return BlueChannelBits + GreenChannelBits + RedChannelBits + AlphaChannelBits; } }
        public double PayloadByteRatio { get { return BitsPerPixel / 32.0; } }

        private void LoadConfig(IDictionary<string, double> config = null)
        {
            double value;

            BlueChannelBits = (int)((config != null && config.TryGetValue(ConfigBlueChannelBits, out value)) ? value : DefaultBlueChannelBits);
            GreenChannelBits = (int)((config != null && config.TryGetValue(ConfigGreenChannelBits, out value)) ? value : DefaultGreenChannelBits);
            RedChannelBits = (int)((config != null && config.TryGetValue(ConfigRedChannelBits, out value)) ? value : DefaultRedChannelBits);
            AlphaChannelBits = (int)((config != null && config.TryGetValue(ConfigAlphaChannelBits, out value)) ? value : DefaultAlphaChannelBits);
        }

        private int GetChannelBits(Stream input)
        {
            var bits = -1;

            switch (input.Position % 4)
            {
                case 0:
                    bits = BlueChannelBits;
                    break;
                case 1:
                    bits = GreenChannelBits;
                    break;
                case 2:
                    bits = RedChannelBits;
                    break;
                case 3:
                    bits = AlphaChannelBits;
                    break;
            }

            return bits;
        }

        private void ReadNextByte(Stream input, ref int inputByte, ref int inputChannelBits, Stream output = null)
        {
            // only write to output if it's provided (not provided for decoding)
            if (output != null)
            {
                // write to output
                output.WriteByte((byte)inputByte);
            }

            // only continue reading if the stream permits it
            if (input.CanRead)
            {
                // get the next input params
                inputChannelBits = GetChannelBits(input);
                inputByte = input.ReadByte();

                // if a channel does not permit encoding, we have to loop until we find the next available channel
                while (input.CanRead && inputChannelBits == 0)
                {
                    // only write to output if it's provided (not provided for decoding)
                    if (output != null)
                    {
                        // write to output
                        output.WriteByte((byte)inputByte);
                    }

                    // get the next input params
                    inputChannelBits = GetChannelBits(input);
                    inputByte = input.ReadByte();
                }
            }
            else
            {
                // if we reached the end of the input stream clear the input params to break out of the main encoding loop
                inputChannelBits = 0;
                inputByte = -1;
            }
        }

        public int GetCapacity(Stream input)
        {
            return (int)Math.Floor(input.Length * PayloadByteRatio);
        }

        public void GetOptimalImageSize(int payloadLength, double aspectRatio, out int width, out int height)
        {
            var pixels = payloadLength / PayloadByteRatio;
            height = (int)Math.Ceiling(Math.Sqrt(pixels / aspectRatio));
            width = (int)Math.Ceiling(aspectRatio * height);
        }

        #region Encode

        public void Encode(Stream input, DynamicLengthStream payload, Stream output)
        {
            if (input.CanRead && output.CanWrite && payload.CanRead)
            {
                var inputChannelBits = GetChannelBits(input);
                var inputByte = input.ReadByte();
                var inputBitOffset = 0;

                while (input.CanRead && output.CanWrite && payload.CanRead)
                {
                    var payloadByte = payload.ReadByte();

                    for (var payloadBitOffset = 0; inputByte >= 0 && output.CanWrite && payloadBitOffset < 8; ++payloadBitOffset)
                    {
                        EncodeBit(input, ref inputByte, ref inputBitOffset, ref inputChannelBits, payloadByte, payloadBitOffset, output);
                    }
                }

                if (input.CanRead == false && payload.CanRead)
                {
                    throw new EndOfStreamException("Input Capacity too Small for Payload");
                }
            }

            // write the remaining input to the output
            if (input.CanRead && output.CanWrite)
            {
                input.CopyTo(output);
            }
        }

        private void EncodeBit(Stream input, ref int inputByte, ref int inputBitOffset, ref int inputChannelBits, int payloadByte, int payloadBitOffset, Stream output)
        {
            // generate the payload bit mask
            var payloadBitMask = 0x01 << payloadBitOffset;

            // mask the payload byte to get the bit value
            var payloadBit = payloadByte & payloadBitMask;

            // shift payload bit to input bit offset
            payloadBit = (payloadBit >> payloadBitOffset) << inputBitOffset;

            // generate the input bit mask
            var inputBitMask = 0x01 << inputBitOffset;

            // clear the input bit from the mask (negation)
            inputByte &= ~inputBitMask;

            // update the input byte with the payload bit
            inputByte |= payloadBit;

            // increment the input bit offset
            inputBitOffset = (inputBitOffset + 1) % inputChannelBits;

            if (inputBitOffset == 0)
            {
                // since we reached the end of our channel encoding bandwidth, write the current byte to output, and read the next input byte
                ReadNextByte(input, ref inputByte, ref inputChannelBits, output);
            }
        }

        #endregion

        #region Decode

        public void Decode(Stream input, DynamicLengthStream payload)
        {
            payload.Position = 0;

            if (input.CanRead && payload.CanWrite)
            {
                var inputChannelBits = GetChannelBits(input);
                var inputByte = input.ReadByte();
                var inputBitOffset = 0;

                while (input.CanRead && payload.CanWrite)
                {
                    var payloadByte = 0;

                    for (var payloadBitOffset = 0; inputByte >= 0 && payload.CanWrite && payloadBitOffset < 8; ++payloadBitOffset)
                    {
                        DecodeBit(input, ref inputByte, ref inputBitOffset, ref inputChannelBits, ref payloadByte, payloadBitOffset);
                    }

                    payload.WriteByte((byte)payloadByte);
                }
            }
        }

        private void DecodeBit(Stream input, ref int inputByte, ref int inputBitOffset, ref int inputChannelBits, ref int payloadByte, int payloadBitOffset)
        {
            // generate the input bit mask
            var inputBitMask = 0x01 << inputBitOffset;

            // mask the input byte to get the bit value
            var inputBit = inputByte & inputBitMask;

            // shift input bit to payload bit offset
            inputBit = (inputBit >> inputBitOffset) << payloadBitOffset;

            // update the payload byte with the input bit (we assume the payload byte starts out at 0x00)
            payloadByte |= inputBit;

            // increment the input bit offset
            inputBitOffset = (inputBitOffset + 1) % inputChannelBits;

            if (inputBitOffset == 0)
            {
                // since we reached the end of our channel encoding bandwidth, read the next input byte
                ReadNextByte(input, ref inputByte, ref inputChannelBits);
            }
        }

        #endregion
    }
}
