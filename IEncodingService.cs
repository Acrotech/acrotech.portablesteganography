﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Acrotech.PortableSteganography
{
    public interface IEncodingService
    {
        Stream OpenRead(Uri uri);
        Stream OpenWrite(Uri uri);

        Uri CreateEncodedUri(Uri hint);
    }
}
